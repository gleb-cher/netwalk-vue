FROM node:alpine AS builder
COPY . /app
WORKDIR /app
RUN npm i && npm run build:demo

FROM nginx:alpine
COPY --from=builder /app/dist /usr/share/nginx/html
EXPOSE 80 443
