# netwalk-vue

Netwalk game (aka pipes, network) vue component. 

![Example](https://gitlab.com/gleb_cher/netwalk-vue/raw/master/public/example.gif)

## DEMO
https://netwalk-vue.gleb-cher.pro/

## Features
- template mode with validation, initial provider and empty cells support
- automatic generation mode with adaptive playing field dimensions
- customizable cell styles (colors, sizes, animations)
- incremental filling and randomizing
- rotation and color change animations
- emit change events for finished, started, moves and stopwatch values

## Installation
```
npm i -S netwalk-vue
```

## Usage
```vue
<template>
    <netwalk />
</template>

<script>
import Netwalk from 'netwalk-vue';

export default {
  name: 'awesome-component',
  components: {
    netwalk: Netwalk,
  },
};
</script>
```

## API
### Component input props
 ```
    fill: {
      type: Boolean,
      default: true
    },
    fillAnimationDelay: {
      type: Number,
      default: 0
    },
    randomize: {
      type: Boolean,
      default: true
    },
    randomizeAnimationDelay: {
      type: Number,
      default: 0
    },
    template: {
      type: Array
    },
    customStyle: {
      type: Object,
      default: {
        cellSizePx: 50,
        connectionWidthPercent: 20,
        connectionColor: '#e1e1e1',
        connectionColorConnected: '#6b8fd4',
        
        cellColor: 'transparent',
        
        emptyCellColor: 'transparent',
        
        providerColor: '#ff6200',
        
        consumerColor: '#ffdc73',
        consumerColorConnected: '#ffbf00',
        
        rotateAnimation: true,
        rotateAnimationDuration: '200ms',
        colorAnimation: true,
        colorAnimationDuration: '150ms'
      }
    }
```


The **automatic adaptive** mode is activated if no template detected.

**Custom styles** merge with defaults.

### Events
Every event fired immediately after component mounting with the changed value.

- started-change
- finished-change
- stopwatch-change
- moves-change
- filling-change
- filled-change
- randomizing-change
- randomized-change


### Game core
Component based on [netwalk-core package](https://www.npmjs.com/package/netwalk-core)

For access to netwalk instance and use netwalk-core API:

```javascript
const instance = mountedComponent.netwalk
```


## Browser support
Tested on last Chrome, Firefox and Safari.

